#!/bin/bash

set -euo pipefail

cd "$(dirname "${BASH_SOURCE[0]}")"

tests/functions.sh
tests/aws_functions.sh
tests/prepare.sh
tests/artifacts.sh
tests/merge.sh
