---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.setup-prepare: |
  # Prepare the testing environment
  # Set setup stage as skipped until we complete it
  rc_state_set stage_setup skip

  # Handle architecture naming differences between test environment and kernel
  export KPET_ARCH="${KPET_ARCH:-${ARCH_CONFIG}}"

.setup-download-bot-artifacts: |
  # ⬇ Download artifacts from previous jobs (if only testing the testing).
  artifact_suffix="${ARCH_CONFIG}"
  if is_true "${DEBUG_KERNEL}"; then
    artifact_suffix="${artifact_suffix}_debug"
  fi
  ARTIFACT_URL_VAR="ARTIFACT_URL_${artifact_suffix}"
  ARTIFACT_JOB_NAME_VAR="ARTIFACT_JOB_NAME_${artifact_suffix}"
  ARTIFACT_JOB_ID_VAR="ARTIFACT_JOB_ID_${artifact_suffix}"
  if [ -n "${!ARTIFACT_URL_VAR:-}" ]; then
    curl --output artifacts.zip "${!ARTIFACT_URL_VAR}?job_token=${CI_JOB_TOKEN}"
    if ! unzip -o artifacts.zip; then
      echo_red "Looks like artifacts expired. Please check ${!ARTIFACT_URL_VAR}"
      exit 1
    fi
    if [ "${artifacts_mode}" = "s3" ]; then
      aws_s3_download_artifact_from_pipeline_job "${ARTIFACT_PIPELINE_ID}" "${!ARTIFACT_JOB_NAME_VAR}" "${!ARTIFACT_JOB_ID_VAR}"
    fi
    # If kcidb_all.json is located in artifacts/, move it to the top level dir.
    # can be removed 2022-01-01 or once pipelines newer than 2021-10-18 exist for all brew pipelines
    if [ -f "artifacts/kcidb_all.json" ]; then
      mv artifacts/kcidb_all.json kcidb_all.json
    fi
    # Replace original kcidb checkout and build ids with ones matching the current pipeline
    original_checkout_id="redhat:${original_pipeline##*/}"
    sed -i "s/${original_checkout_id}/${KCIDB_CHECKOUT_ID}/g" kcidb_all.json
    # Update important fields to match the *current* pipeline variables
    # some fields like misc.pipeline.variables are not updated at the moment!
    kcidb_edit_checkout set-bool misc/retrigger "${retrigger}"
    kcidb_edit_checkout set-bool misc/scratch "${is_scratch}"
    # Call cki.kcidb.adapter to fix GL job/pipeline variables
    dump_kcidb_data "${VENV_PY3}" checkout "${KCIDB_CHECKOUT_ID}"
  fi

.setup-generate-test-xml: |
  # 🎰 Generate the test XML using kpet.
  # smoke test the kpet module; all invocations later on happen outside set -e
  "${VENV_PY3}" -m kpet --help > /dev/null 2>&1
  # Get the path to the build URL.
  KERNEL_URL=$(rc_state_get kernel_package_url)
  if [ -z "${KERNEL_URL}" ] ; then
    echo "Kernel URL is missing!"
    exit 1
  fi
  # Set the KERNEL_RELEASE environment variable so that it appears in the XML
  # output from KPET.
  export KERNEL_RELEASE
  KERNEL_RELEASE=$(rc_state_get kernel_version)
  # Set KPET_SETS to the default from trigger configuration, or to all sets
  KPET_SETS=${test_set:-.*}
  # Match user suffix "test-cki.suffix" against kpet set list
  _TMP_TEST_SET=$(echo "${KERNEL_RELEASE}" | grep -oP "test[-\.]cki\.\K(\w*)" || true)

  # The suffix has to match exactly
  _TMP_TEST_SET="$(run_logged "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" set list | cut -f1 -d " " | grep -w "${_TMP_TEST_SET}" || true)"
  if [ -n "${_TMP_TEST_SET}" ]; then
      KPET_SETS=${_TMP_TEST_SET}
  fi

  # Override test set when using AWS runner, as it can only run VM tests.
  if [ "${test_runner}" == "aws" ] ; then
    KPET_SETS="(${KPET_SETS}) & cloud"
  fi

  # Get zstream/ystream information about RHEL kernels
  if [ -n "${kpet_tree_name}" ]; then
    echo_yellow "Manual kpet tree override specified: ${kpet_tree_name}"
  else
    kpet_tree_name=$("${VENV_PY3}" -m cki.cki_tools.select_kpet_tree "${kpet_tree_family}" "${KERNEL_RELEASE}")
    echo "Using kpet tree ${kpet_tree_name} determined from family ${kpet_tree_family} and nvr ${KERNEL_RELEASE}"
  fi
  rc_state_set kpet_tree_name "${kpet_tree_name}"
  kcidb_edit_build set misc/kpet_tree_name "${kpet_tree_name}"
  kcidb_edit_build set misc/kpet_tree_family "${kpet_tree_family}"

  # Generate a regular expression matching kpet-db's extra build components
  # If testing a standalone package/image
  if [[ ${KERNEL_URL} =~ \.(rpm|tar\.gz)$ ]]; then
    # Match no extra build components
    COMPONENTS_RE=""
  else
    # Generate a regex matching detected extra build components
    # (list packages, convert matching packages to component names, remove
    # duplicates, strip trailing newline, join lines with '|')
    # shellcheck disable=SC2046 # FIXME warning disabled to enable linting: warning: Quote this to prevent word splitting. [SC2046]
    COMPONENTS_RE=$(paste -s -d'|' <<<$(
      loop dnf repoquery --disablerepo='*' --repofrompath "cki,${KERNEL_URL}" |
          awk '
            /^kernel-headers-/          {print "headers"}
            /^kernel-devel-/            {print "devel"}
            /^kernel-debug-/            {print "debugbuild"}
            /^kernel-debuginfo-/        {print "debuginfo"}
            /^kernel-abi-whitelists-/   {print "abi"}
            /^kernel-abi-stablelists-/  {print "abi"}
            /^perf-/                    {print "tools"}
          ' | sort -u
    ))

    # If this is an official build, mark it as such. We can safely add "|officialbuild"
    # as official builds will always have a complete repo which matches some components
    # from the list above.
    if ! is_true "${is_scratch}"; then
      COMPONENTS_RE="${COMPONENTS_RE}|officialbuild"
    fi
  fi
  if [[ ! ${COMPONENTS_RE} =~ "debugbuild" ]]; then
    # check if the config used to build the kernel has debug flag enabled
    CONFIG_FILE=$(rc_state_get config_file)
    if [ -e "${CONFIG_FILE}" ]; then
      # Check for some debug options that can cause performance issues
      # flags associated with a debug kernel causes https://gitlab.com/cki-project/kernel-tests/-/issues/657,
      if grep -qwE "CONFIG_LOCKDEP=y|CONFIG_DEBUG_OBJECTS=y" "${CONFIG_FILE}"; then
        # Kernel was built using debug flags
        # only run tests enabled for debug kernel builds
        if [ -z "${COMPONENTS_RE}" ] ; then
           COMPONENTS_RE="debugbuild"
        else
          COMPONENTS_RE="${COMPONENTS_RE}|debugbuild"
        fi
      fi
    fi
  fi
  echo "Generated build component regex for kpet: \"${COMPONENTS_RE}\""

  # Verify the architecture is supported with the stream
  if run_logged "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" arch list -t "${kpet_tree_name}" "${KPET_ARCH}" | grep -q .; then
    echo "${KPET_ARCH} is supported for ${kpet_tree_name}"
  else
    echo "${KPET_ARCH} is not supported for ${kpet_tree_name}"
    rc_state_set retcode 0
    rc_state_set reason unsupported
    kcidb_edit_build set misc/testing_skipped_reason "unsupported"
    kcidb_edit_build set-bool misc/test_plan_missing false
    exit 0
  fi

  # Assemble parameters to add to the kernel URL for kpkginstall to use.
  # Always add package_name
  KERNEL_URL_PARAMS=("package_name=${package_name}")
  # Add the debug_kernel parameter if we are testing a debug kernel.
  if is_true "${DEBUG_KERNEL}"; then
    KERNEL_URL_PARAMS+=("debug_kernel=true")
  fi

  # If we have parameters for the kernel URL, join them together and append
  # them to the kernel url.
  if [ ${#KERNEL_URL_PARAMS[@]} -gt 0 ]; then
      # The & -> &amp; conversion is required since valid XML cannot deal with
      # bare ampersands.
      JOINED_KERNEL_URL_PARAMS=$(join_by \& "${KERNEL_URL_PARAMS[@]}" | sed 's/&/&amp;/g')
      echo "Appending kernel URL parameters: ${JOINED_KERNEL_URL_PARAMS}"
      KERNEL_URL="${KERNEL_URL}#${JOINED_KERNEL_URL_PARAMS}"
      echo "New kernel URL: ${KERNEL_URL}"
  fi

  # Ensure that _debug is added to the architecture string if we are testing a
  # debug kernel.
  if is_true "${DEBUG_KERNEL}"; then
    ARCH_STRING="${KPET_ARCH}_debug"
  else
    ARCH_STRING="${KPET_ARCH}"
  fi

  # If we have testing pipeline for a merge request, we need to pass the dummy
  # patch to kpet and targeted testing checker.
  if [ -n "${mr_id}" ] ; then
    patch_urls="${CI_PROJECT_DIR}/${MR_DIFF_PATH}"
  fi

  echo "KPET_SETS is: ${KPET_SETS}"
  kpet_generate_variable_arguments

  kpet_run_noarch_arguments=(
    --tree "${kpet_tree_name}"
    --components "${COMPONENTS_RE}"
    --sets "${KPET_SETS}"
    --high-cost "${kpet_high_cost}"
    ${tests_regex:+--tests "${tests_regex}"}
  )
  kpet_run_multiarch_arguments=("${kpet_run_noarch_arguments[@]}")
  kpet_run_arguments=("${kpet_run_noarch_arguments[@]}" --arch "${KPET_ARCH}")


  # Skip unsupported architectures we have to build to verify Brew builds don't break.
  # These are not supported by kpet and it gets very upset if we pass such values,
  # so we need to skip them for the multiarch command.
  mapfile -t architecture_array < <(
    "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" arch list -t "${kpet_tree_name}" \
      "$(sed -e 's/^ \+//; s/ \+$//; s/ \+/|/g' <<<"${architectures}")"
  )
  read -r -a domain_array <<<"${domains}"
  for ((i = 0; i < ${#domain_array[@]}; i++)); do
    if ((i > 0)); then
      kpet_run_arguments+=("--execute")
    fi
    kpet_run_arguments+=("--domains=${domain_array[i]}")
    for ((j = 0; j < ${#architecture_array[@]}; j++)); do
      if ((i + j > 0)); then
        kpet_run_multiarch_arguments+=("--execute")
      fi
      if ((j == 0)); then
        kpet_run_multiarch_arguments+=("--domains=${domain_array[i]}")
      fi
      kpet_run_multiarch_arguments+=("--arch=${architecture_array[j]}")
    done
  done

  if [ -z "${KPET_SETS}" ]; then
    echo "KPET_SETS is empty"
  else
    # Run kpet to generate beaker XML.
    # shellcheck disable=SC2086 # disabled on purpose as we want patch_urls to expand
    run_logged "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" run generate \
      -v "job_group=cki" \
      -v selftests_url="$(rc_state_get selftests_url)" \
      "${kpet_variable_arguments[@]}" \
      -k "${KERNEL_URL}" \
      -d "cki@gitlab:${CI_PIPELINE_ID} ${KERNEL_RELEASE}@${CI_COMMIT_REF_NAME} ${ARCH_STRING}" \
      -o "${BEAKER_TEMPLATE_PATH}" \
      "${kpet_run_arguments[@]}" \
      ${patch_urls:-}
  fi

  beaker_xml="$(artifact_url "${BEAKER_TEMPLATE_PATH}")"
  echo_green "Generated test XML available in the job artifacts as ${beaker_xml}"
  kcidb_edit_build append-dict output_files name=beaker_xml url="${beaker_xml}"

.setup-get-modified-files: |
  # Get a list of modified files from patches and save them to state file
  if [ -n "${patch_urls:-}" ] ; then
    # shellcheck disable=SC2086 # disabled on purpose as we want patch_urls to expand
    "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" patch list-files ${patch_urls} | tr "\n" " " | \
      "${VENV_PY3}" -m cki.cki_tools.rc_edit --stdin "${RC_FILE}" rc_state_set modified_files
    read -r -a modified_files <<< "$(rc_state_get modified_files)"
    for modified_file in "${modified_files[@]}"; do
      kcidb_edit_checkout append-dict misc/patchset_modified_files path="${modified_file}"
    done
  fi

.setup-targeted: |
  # 🎯 Check whether this test run can have, and has any targeted tests/sources
  # Assume we cannot have targeted tests, first
  # TODO: Consider removing once/if everything switches to "all_sources_targeted"
  rc_state_set targeted_tests -1
  # Assume we don't have any sources, first
  rc_state_set all_sources_targeted -1
  # If we have patches
  if [ -n "${patch_urls:-}" ]; then
    # If we have changed sources we can target
    # shellcheck disable=SC2086 # disabled on purpose as we want patch_urls to expand
    SOURCES=$(
      "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" \
                            patch list-files --sources ${patch_urls}
    )
    if [ -n "${SOURCES}" ]; then
      # We can have targeted tests, but none are found yet
      rc_state_set targeted_tests 0
      # shellcheck disable=SC2086 # disabled on purpose as we want patch_urls to expand
      run_logged "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" run test list \
        "${kpet_run_multiarch_arguments[@]}" --targeted=yes -- ${patch_urls} \
        > "${TARGETED_TESTS_PATH}"
      # If we have targeted tests
      if [ -s "${TARGETED_TESTS_PATH}" ]; then
        rc_state_set targeted_tests 1
        rc_state_set targeted_tests_list "${TARGETED_TESTS_PATH}"
      fi
      # If we have some untargeted sources
      UNTARGETED_SOURCES=$(
        # shellcheck disable=SC2086 # disabled on purpose as we want patch_urls to expand
        run_logged "${VENV_PY3}" -m kpet --db "${SOFTWARE_DIR}/kpet-db" run source list \
          "${kpet_run_multiarch_arguments[@]}" --targeted=no -- ${patch_urls}
      )
      if [ -n "${UNTARGETED_SOURCES}" ]; then
        rc_state_set all_sources_targeted 0
        echo_yellow "Some source files are not targeted by any tests:"
        echo_yellow "${UNTARGETED_SOURCES}"
      else
        rc_state_set all_sources_targeted 1
      fi
    fi
  fi

.setup-generate-test-plan: |
  # Generate test plan !
  if ! [ "${test_mode:-}" == "skt" ] && ! is_true "${skip_test}"; then
    # Test plan is not supported by SKT pipelines
    "${VENV_PY3}" -m cki.kcidb.beaker_to_kcidb \
      --beaker-xml "${BEAKER_TEMPLATE_PATH}" \
      --kcidb-file "${KCIDB_DUMPFILE_NAME}" \
      --build-id "${KCIDB_BUILD_ID}"
    kcidb_edit_build set-bool misc/test_plan_missing false
  fi

# NOTE(mhayden): The `setup` stage allows us to potentially submit a job for
# testing in an external CI system while the normal CKI jobs run in the `test`
# stage.
.setup:
  extends: [.with_artifacts, .with_retries, .with_python_image]
  stage: setup
  variables:
    ARTIFACTS: "artifacts/*.xml ${TARGETED_TESTS_PATH}"
  before_script:
    - !reference [.common-before-script]
  script:
    - !reference [.setup-prepare]
    - !reference [.setup-download-bot-artifacts]
    - !reference [.setup-generate-test-xml]
    - !reference [.setup-get-modified-files]
    - !reference [.setup-targeted]
    - rc_state_set stage_setup pass
    - !reference [.setup-generate-test-plan]
    - aws_s3_upload_artifacts
  after_script:
    - !reference [.common-after-script-head]
    - |
      if [[ "${CI_JOB_STATUS}" == "failed" ]]; then
        aws_s3_upload_artifacts
      fi
    - !reference [.common-after-script-tail]
  tags:
    - pipeline-setup-runner
  rules:
    - !reference [.skip_without_stage]
    - !reference [.skip_without_debug]
    - !reference [.skip_without_architecture]
    - when: on_success

setup i686:
  extends: [.setup, .i686_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish i686
  dependencies:
    - prepare python
    - publish i686

setup x86_64:
  extends: [.setup, .x86_64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish x86_64
  dependencies:
    - prepare python
    - publish x86_64

setup x86_64 debug:
  extends: [.setup, .x86_64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish x86_64 debug
  dependencies:
    - prepare python
    - publish x86_64 debug

setup ppc64le:
  extends: [.setup, .ppc64le_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish ppc64le
  dependencies:
    - prepare python
    - publish ppc64le

setup aarch64:
  extends: [.setup, .aarch64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish aarch64
  dependencies:
    - prepare python
    - publish aarch64

setup ppc64:
  extends: [.setup, .ppc64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish ppc64
  dependencies:
    - prepare python
    - publish ppc64

setup s390x:
  extends: [.setup, .s390x_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      publish s390x
  dependencies:
    - prepare python
    - publish s390x
